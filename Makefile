coverage: coverage.html

coverage.html: coverage-report

generate-coverage:
	cargo tarpaulin --out Xml

coverage-report: generate-coverage
	pycobertura show --format html --output coverage.html cobertura.xml

clean:
	cargo clean
	rm cobertura.xml
	rm coverage.html