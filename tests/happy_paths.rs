use naive_json_parser::JSONValue::{Array, False, Null, Number, Object, True};
use naive_json_parser::*;
use std::collections::HashMap;
use std::convert::TryFrom;
use std::f64::consts::PI;

#[test]
fn value_conversion() -> Result<(), &'static str> {
    let map: JSONMap = HashMap::new();
    let num = 9.380831539;
    let str = "applesauce";
    let arr: JSONArray = vec![
        JSONValue::from(map.clone()),
        JSONValue::from(num),
        JSONValue::from(str),
    ];

    assert_eq!(map.clone(), JSONMap::try_from(JSONValue::from(map.clone()))?);
    assert_eq!(num, f64::try_from(JSONValue::from(num))?);
    assert_eq!(String::from(str), String::try_from(JSONValue::from(str))?);
    assert_eq!(arr.clone(), JSONArray::try_from(JSONValue::from(arr.clone()))?);
    assert_eq!(true, bool::try_from(JSONValue::from(true))?);
    assert_eq!(false, bool::try_from(JSONValue::from(false))?);
    assert_eq!((), <()>::from(JSONValue::from(())));

    Ok(())
}

#[test]
fn wrap_and_unwrap() {
    let map: JSONMap = HashMap::new();
    let num = 9.380831539;
    let str = "applesauce";
    let arr: JSONArray = vec![
        JSONValue::from(map.clone()),
        JSONValue::from(num),
        JSONValue::from(str),
    ];

    let s: String = JSONValue::from(str).unwrap();
    let a: JSONArray = JSONValue::from(arr.clone()).unwrap();

    assert_eq!(map.clone(), JSONValue::from(map.clone()).unwrap());
    assert_eq!(num, JSONValue::from(num).unwrap());
    assert_eq!(str, &s);
    assert_eq!(arr.clone(), a);
    assert_eq!(true, JSONValue::from(true).unwrap());
    assert_eq!(false, JSONValue::from(false).unwrap());
    assert_eq!((), JSONValue::from(()).unwrap());
}

#[test]
fn sanity_check() {
    let res = JSON::parse(r#""foo""#);
    assert_eq!(res, Ok(JSONValue::String(String::from("foo"))));

    let json = format!("{}", PI);
    let res = JSON::parse(&json);
    assert_eq!(res, Ok(JSONValue::Number(PI)));
}

#[test]
fn can_parse_array_of_keywords() {
    let result = JSON::parse("[true,false,null]");

    assert_eq!(result, Ok(Array(vec![True, False, Null])));
}

#[test]
fn parse_json_types() {
    // Boolean / Null
    let res = JSON::parse("true");
    assert_eq!(res, Ok(True));
    let res = JSON::parse("false");
    assert_eq!(res, Ok(False));
    let res = JSON::parse("null");
    assert_eq!(res, Ok(Null));

    // Number
    let res = JSON::parse("9.38083151965");
    assert_eq!(res, Ok(Number(9.38083151965)), "Failed to parse number");

    // String
    let res = JSON::parse(r#""/^$/""#);
    assert_eq!(res, Ok(JSONValue::from("/^$/")), "Failed to parse string");

    // Number array
    let res = JSON::parse("[1, 2, 3]");
    assert_eq!(
        res,
        Ok(Array(vec![Number(1f64), Number(2f64), Number(3f64)]))
    );

    // Object array
    let result = JSON::parse("[{}]");
    assert_eq!(result, Ok(JSONValue::Array(vec![Object(HashMap::new())])));
}

#[test]
fn parse_nested_object() {
    let res = JSON::parse(r#"{"a": {"b": []}}"#);
    let mut outermap: JSONMap = HashMap::new();
    let mut innermap: JSONMap = HashMap::new();

    innermap.insert(String::from("b"), Array(vec![]));
    outermap.insert(String::from("a"), Object(innermap));

    let expected = Ok(Object(outermap));

    assert_eq!(res, expected);
}

#[test]
fn parse_object_with_number_values() {
    let result = JSON::parse(r#"[{ "a": 9.38083151965, "b": 4e3 }]"#);
    let mut map: JSONMap = HashMap::new();
    map.insert(String::from("a"), Number(9.38083151965f64));
    map.insert(String::from("b"), Number(4e3f64));

    let expected = Ok(Array(vec![Object(map)]));

    assert_eq!(
        result, expected,
        "Failed on just number values: {:#?}",
        result
    );
}

#[test]
fn parse_weird_character_array() {
    let result =
        JSON::parse(r#"["\"", "\\", "/", "\b", "\f", "\n", "\r", "\t", "\u0001", "\uface"]"#);
    let expected = Ok(Array(vec![
        JSONValue::from("\""),
        JSONValue::from("\\"),
        JSONValue::from("/"),
        JSONValue::from("\u{8}"),
        JSONValue::from("\x0C"),
        JSONValue::from("\n"),
        JSONValue::from("\r"),
        JSONValue::from("\t"),
        JSONValue::from("\u{1}"),
        JSONValue::from("\u{face}"),
    ]));

    assert_eq!(result, expected);
}

#[test]
fn parse_full_json_example() {
    let result = JSON::parse(
        r#"[{
  "a": 9.38083151965,
  "b": 4e3,
  "c": [1, 2, 3],
  "d": "foo",
  "e": {
    "f": {
      "g": {
        "h": null
      }
    }
  },
  "i": ["\"", "\\", "/", "\b", "\f", "\n", "\r", "\t", "\u0001", "\uface"]
}]"#,
    );

    let mut map: JSONMap = HashMap::new();
    let mut emap: JSONMap = HashMap::new();
    let mut fmap: JSONMap = HashMap::new();
    let mut gmap: JSONMap = HashMap::new();

    gmap.insert(String::from("h"), Null);
    fmap.insert(String::from("g"), Object(gmap));
    emap.insert(String::from("f"), Object(fmap));

    map.insert(String::from("a"), Number(9.38083151965f64));
    map.insert(String::from("b"), Number(4e3f64));
    map.insert(
        String::from("c"),
        Array(vec![Number(1f64), Number(2f64), Number(3f64)]),
    );
    map.insert(String::from("d"), JSONValue::from("foo"));
    map.insert(String::from("e"), Object(emap));

    map.insert(
        String::from("i"),
        Array(vec![
            JSONValue::from("\""),
            JSONValue::from("\\"),
            JSONValue::from("/"),
            JSONValue::from("\u{8}"),
            JSONValue::from("\x0C"),
            JSONValue::from("\n"),
            JSONValue::from("\r"),
            JSONValue::from("\t"),
            JSONValue::from("\u{1}"),
            JSONValue::from("\u{face}"),
        ]),
    );

    assert!(result.is_ok(), format!("{:#?}", result));

    let outer_array: Vec<JSONValue> = result.unwrap().unwrap();
    let result_map: JSONMap = outer_array[0].clone().unwrap();

    for (k, v) in &map {
        assert_eq!(
            result_map.get(k).unwrap(),
            v,
            "HashMap Entry Differs: {:#?}, {:#?}",
            result_map.get(k).unwrap(),
            v
        );
    }
}
