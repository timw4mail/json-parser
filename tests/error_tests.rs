use naive_json_parser::*;

#[test]
fn sanity_check() {
    let res = JSON::parse(r#"-q"#);
    assert_eq!(
        res,
        Err(ParseError::ExpectedDigit(String::from(
            "Expected a digit, received 'q' after numeric '-'"
        )))
    );
}

#[test]
fn bad_object_trailing_comma() {
    let res = JSON::parse("{,}");
    assert_eq!(
        res,
        Err(ParseError::ExpectedObjectKey(String::from(
            "Expected an object key. Does the object have a trailing comma?"
        )))
    );
}

#[test]
fn bad_json() {
    let res = JSON::parse(r#"5eq"#);
    assert!(res.is_err());
    println!("{:#?}", res);
}
